﻿window.addEventListener('load', function()
{
	/*Alexander*/
	var Username = '0008';
	var UserPass = '1346';

	/*Andres*/
	//var Username = '0011';
	//var UserPass = '1678';

	var currentYear = new Date().getFullYear();
	var currentMonth = new Date().getMonth() + 1;
	var currentDay = new Date().getDate();

	/* Aktuelle Anzahl Monatstage herausfinden */
	function CalcMonthDays()
	{
		return new Date(currentMonth, currentYear, 0).getDate();
	}

	function minToHourMin(minutes)
	{
		if(minutes === null)
		{
			return '';
		}
		var min = false;
		if(minutes < 0)
		{
			min = true;
			minutes = minutes * -1;
		}
		var h = Math.floor(minutes / 60);
		var m = minutes % 60;
		return (min ? '-' : '') + (h <= 9 ? '0' : '') + h + ':' + (m <= 9 ? '0' : '') + m;
	}

	function getDateData(dateCount)
	{
		var DateString = (dateCount <= 9 ? '0' + dateCount : dateCount) + '.' + (currentMonth <= 9 ? '0' + currentMonth : currentMonth) + '.' + currentYear;
		var Url = 'https://zeit.abf.ch:44301/Service.svc/GetAuswertungForPnr/DATUM/' + DateString + '/PNR/' + Username;
		var AuthCode = b64_hmac_md5(UserPass, Url);
		$.ajax({
			method: 'POST',
			url: Url,
			crossDomain: true,
			async: true,
			dataType: 'json',
			headers: {
				'Authorization': AuthCode,
				'username': Username,
				'mandant': '00900',
				'devicetoken': '4eced8e9f30b33c7e9ec9f863c50f09294f949bf1fd681fb0711d5316aa49a59',
				'Content-Type': 'application/json',
				'cache-control': 'no-cache'
			},
			success: function(data)
			{
				calcDateData(data, dateCount);
			}
		});
	}

	function calcDateData(elem, dateCount)
	{
		var startTime = elem[0].Beginn;
		var timeSoll = elem[0].Sollzeit;
		var endTime = elem[elem.length - 1].Ende;
		if(endTime == -1)
		{
			endTime = null;
		}
		var date = elem[0].Tag.split(' ')[0];
		var breakMorning = 0;
		var breakAfternoon = 0;
		var lunchStart = 0;
		var lunchEnd = 0;
		for(var i = 0; i < elem.length; i++)
		{
			// Früher als 11:30
			if(elem[i].Beginn < 690 && elem[i].Pause > 0)
			{
				breakMorning = breakMorning + elem[i].Pause;
			}//Mittag
			if(elem[i].Beginn >= 690 && elem[i].Beginn <= 810)
			{
				lunchEnd = elem[i].Beginn;
			}//Mittag
			if(elem[i].Ende >= 690 && elem[i].Ende <= 810)
			{
				lunchStart = elem[i].Ende;
			}
			// Später als 13:30
			if(elem[i].Beginn > 810 && elem[i].Pause > 0)
			{
				breakAfternoon = breakAfternoon + elem[i].Pause;
			}
		}
		var currentMinutes = (new Date().getMinutes() + 1) * (new Date().getHours() + 1);
		var timeIst = currentMinutes - startTime - breakMorning - breakAfternoon - (lunchEnd - lunchStart);
		$('#Rows #Row-' + currentDay)[0].innerHTML = '<td>'+date+'</td> <td>'+
			minToHourMin(startTime)+'</td> <td>'+
			minToHourMin(breakMorning)+'</td> <td>'+
			minToHourMin(lunchStart)+'</td> <td>'+
			minToHourMin(lunchEnd)+'</td> <td>'+
			minToHourMin(breakAfternoon)+'</td> <td>'+
			minToHourMin(endTime)+'</td> <td> ' +
			minToHourMin(timeSoll) + '</td> <td> '+
			minToHourMin(timeIst) + '</td> <td> '+ 
			minToHourMin(timeIst - timeSoll) + '</td>';
	}

	$('#Rows').append('<tr id="Row-' + currentDay + '"><td>Loading...</td></tr>');
	getDateData(currentDay);
});